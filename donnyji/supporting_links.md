# Emails 
bossdonnyji@gmail.com 
codedis213@gmail.com

# django admin autocomplete on ForeignKey  
https://docs.djangoproject.com/en/2.0/ref/contrib/admin/#django.contrib.admin.ModelAdmin.autocomplete_fields
https://stackoverflow.com/questions/9793707/django-admin-change-selected-box-of-related-fields-to-autocomplete

# commands to delete all migration files at once 
https://simpleisbetterthancomplex.com/tutorial/2016/07/26/how-to-reset-migrations.html

find . -path "*/migrations/*.py" -not -name "__init__.py" -delete
find . -path "*/migrations/*.pyc"  -delete

# find and delete all pyc and env from git 
find . -name '*.pyc' | xargs -n 1 git rm --cached
find . -name '*env' | xargs -n 1 git rm -rf  --cached

# install headless selenium 
https://selenium-python.readthedocs.io/
https://stackoverflow.com/questions/48537028/selenium-how-to-use-headless-chrome-on-aws

# geckodriver' executable needs
selenium.common.exceptions.WebDriverException: Message: 'geckodriver' executable needs to be in PATH. 
sudo apt-get install firefox-geckodriver

from selenium import webdriver
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary

$ which geckodriver
/usr/bin/geckodriver
$ which firefox
/usr/bin/firefox

binary = FirefoxBinary('/usr/bin/firefox')
browser = webdriver.Firefox(firefox_binary=binary)
browser.get(url)


# dumpdata 
python manage.py dumpdata scrap_app.ProductDetailsExtractions --indent 2
--format json >
scrap_app/management/commands/ProductDetailsExtractions.json
 
python manage.py dumpdata seller.SellerAddressLatLon --indent 2 --format
json > scrap_app/management/commands/SellerAddressLatLon.json

# some git commands  

git fetch;git reset --hard origin/<branch>
git pull --rebase origin <branch>
git fetch; git remote update origin --prune
git checkout <branch>
git pull --rebase origin master;

# resolve conflicts
git rebase –-continue
git rr-commit
git rr-push

python manage.py  makemigrations
python manage.py migrate
python manage.py  test -v2

git push origin <branch> --force

git al-merge  <branch> integration

git recreate-branch integration --list

exclude branch 
git recreate-branch staging -x <branch> 
git push origin staging -f

reset directory 
git squash 
git rebase -i HEAD~14

# git cherry pick
git log
git reset --hard origin/<base branch>
git cherry-pick afa99fef35a715f63dc292d062ab4b1c768a47fb
git commit
git push origin <branch> -f

git log
git reset --hard origin/GPQL-3/applicants-api
git cherry-pick afa99fef35a715f63dc292d062ab4b1c768a47fb
git commit
git push origin ATSI-823-to-825-No-Validations-in-Status-field-for-GraphQL -f



# insert product details into db 
python manage.py load_product_details load_from file 
python manage.py load_seller_n_store_details load_from file
python manage.py load_seller_into_products load_from file

# django rest api, how to recursively retrieve foreign key records

class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ('id', 'name', 'parentid', 'subcategories')

    def get_fields(self):
        fields = super(CategorySerializer, self).get_fields()
        fields['subcategories'] = CategorySerializer(many=True)
        return fields
        
https://www.django-rest-framework.org/api-guide/relations/
https://stackoverflow.com/questions/13376894/django-rest-framework-nested-self-referential-objects

# jquery on click on the dynamic element 
https://stackoverflow.com/questions/9484295/jquery-click-not-working-for-dynamically-created-items

# django create Q object ouside the filter 
http://bradmontgomery.blogspot.com/2009/06/adding-q-objects-in-django.html

q_object = Q()
q_object.add(Q(), Q.AND)

# ORing Q objects
q_object = Q()
q_object.add(Q(), Q.OR)

q = Q(content__icontains=term_list[0]) | Q(title__icontains=term_list[0])
for term in term_list[1:]:
    q.add((Q(content__icontains=term) | Q(title__icontains=term)), q.connector)
    
# indentaion python project directory 
autopep8 ../donnyji  --recursive --select=E101,E121 --in-place

# jquery price slider 
https://www.sitepoint.com/4-jquery-mobile-price-sliders-range-select/
https://jqueryui.com/slider/#range


# django annotate, type cast, string integer into int 
from django.db.models import FloatField 
from django.db.models.functions import Cast 

Product.objects.annotate(actual_price_as_float=Cast( 'actual_price',
FloatField()), sale_price_as_float=Cast('sale_price',
FloatField())).filter(category__category_name__icontains = "School
Bags", sale_price_as_float__range = (100, 2000))

# jquery apply-a-function-to-multiple-elements-with-the-same-id
https://www.sitepoint.com/community/t/how-to-apply-a-function-to-multiple-elements-with-the-same-id/22521


 $("#category_product_text").on("click",function(e){
   var img_tag = $(this);
   console.log(img_tag.attr("prodid"))
   window.location.href = "/product_detail/" + img_tag.attr("prodid");

  })
  

# jquery apply-a-function-to  dynamic content having multiple-elements-with-the-same-id
https://www.sitepoint.com/community/t/how-to-apply-a-function-to-multiple-elements-with-the-same-id/22521


 $('body').on('click', '#category_product_text', function () {
		var img_tag = $(this);
		console.log(img_tag.attr("prodid"))
		window.location.href = "/product_detail/" + img_tag.attr("prodid");

	});
  

# django handling url having <pk> in template and class-based view 

    urls.py 
    
    url(r'^single_product/(?P<pk>\d+)/$', product_detail, name='single_product'),
    
    
    views.py 
    
    def product_detail(self, request, *args, **kwargs):
        pk  = kwargs.get("pk")
        
    template
    {% url 'core:product_detail' product.id %}
    
# django dumpdata ad loaddata 

https://coderwall.com/p/mvsoyg/django-dumpdata-and-loaddata
./manage.py dumpdata > db.json
./manage.py loaddata user.json

# css cursor property 
https://www.w3schools.com/cssref/pr_class_cursor.asp
.pointer {cursor: pointer;}

# Convert “unknown format” strings to datetime objects

import dateutil.parser as parser
parser.parse("2019-12-08T13:17:42.545300+00:00").strftime("%Y-%m-%d %I:%M %p")


# custom tag in django template 
https://docs.djangoproject.com/en/3.0/howto/custom-template-tags/

# bootstrap model show on click of button 
https://www.w3schools.com/bootstrap/tryit.asp?filename=trybs_modal&stacked=h

# csrf token handling in the html templatte and jquery in django 
https://docs.djangoproject.com/en/3.0/ref/csrf/

# How to truncate a foreign key constrained table?
SET FOREIGN_KEY_CHECKS = 0; 
TRUNCATE table $table_name; 
SET FOREIGN_KEY_CHECKS = 1;

# Django Migrations to recreate the deleted tables
python manage.py makemigrations <app-name>
python manage.py sqlmigrate <app-name> 0001_initial

then you will get the queries on console. just copy and paste it in the mysql 

ALTER TABLE `communication_complaintchat` ADD CONSTRAINT `communication_compla_user_id_id_d14c900e_fk_account_u` FOREIGN KEY (`user_id_id`) REFERENCES `account_user` (`id`);


# How to set a value of a variable inside a template code?

https://stackoverflow.com/questions/1070398/how-to-set-a-value-of-a-variable-inside-a-template-code

{% with name="World" %}     
<html>
<div>Hello {{name}}!</div>
</html>
{% endwith %}

# clear input fields on page refresh (Microsoft Edge)
<input autocomplete="off">

# What is the best way to test for an empty string with jquery-out-of-the-box?
if (!a.trim()) {
    // is empty or whitespace
}

if (!a) {
  // is emtpy
}

# How to access jQuery serialized data?

function makeDict(data) {
				var values = {}
				$.each(data, function (i, field) {
					values[field.name] = field.value;
				});
				return values

			}


data_dict = makeDict($('#form_add_review').serializeArray())
rating = data_dict["rating"]
message = data_dict["message"]

# Limiting floats to two decimal points
avg_rating = 15.9499990009999999
"{0:.2f}".format(avg_rating)






